const request = require('request')

/**
 * slackNotif function.
 *
 * Méthode qui sert écrire dans slack
 *
 */
export default async function slackNotif(message, mode: 'alerte' | 'success' | 'infos' = 'alerte'): Promise<any> {
  let msg = message
  if ( mode === "alerte"){
    msg = " 🔥🔥🔥 ALERTE 🔥🔥🔥 " + message
  }
  else if (mode === "success"){
    msg = " 🥳🥳🥳 SUCCESS 🥳🥳🥳 " + message
  }
  else if (mode === "infos"){
    msg = " 🧙🧙🧙 INFOS 🧙🧙🧙 " + message
   //
  }
  console.log(process.env.GCP_PROJECT)
      let url = "https://hooks.slack.com/services/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"
  if (process.env.GCP_PROJECT === "firestore-angular-starter"){
      url = 'https://hooks.slack.com/services/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'
  }

  return request.post(url,
  { json:
    {"text":msg}
  })
}
