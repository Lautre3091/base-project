import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {Route} from '../shared/services/route';
import {UserComponent} from './components/user/user.component';


// Include route guard in routes array
const routes: Routes = [
  Route.withShellAuth([
      {path: '', redirectTo: 'user', pathMatch: 'full'},
      {path: 'user', component: UserComponent},
    ]
  )];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class DashboardRoutingModule {
}
