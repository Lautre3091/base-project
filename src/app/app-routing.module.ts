import {NgModule} from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';


// Include route guard in routes array
const routes: Routes = [{
  path: 'dashboard',
  loadChildren: () => import('./dashboard/dashboard.module').then((m) => m.DashboardModule)
}, {
  path: 'admin',
  loadChildren: () => import('./admin/admin.module').then((m) => m.AdminModule)
}, {
  path: 'clients',
  loadChildren: () => import('./clients/clients.module').then((m) => m.ClientsModule)
}];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules, enableTracing: false})],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
