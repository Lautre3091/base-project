import { Component, OnInit } from '@angular/core';
import {ClientService} from '../../services/client.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Client} from '../../../shared/interfaces/client';
import {switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.css']
})
export class EditClientComponent implements OnInit {
  private id: string;
  public client: Client;


  constructor(
    private clientService: ClientService,
    private router: Router,
    private route: ActivatedRoute
              ) { }


  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap( params => {
        this.id = params.get('id');
        return this.clientService.get(+this.id);
      })
    ).subscribe( (data) => this.client = data );
  }

  editClient(data: any) {
    console.log(data);
    // rattacher l'id
    data.id = this.client.id ;
    this.clientService.update(data).subscribe(
      () => this.router.navigate(['/clients', 'list'])
    );
  }
}
