import { Component } from '@angular/core';
import {NgxUiLoaderService} from 'ngx-ui-loader';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  config: any;

  constructor(
    private ngxUiLoaderService: NgxUiLoaderService,
    private translate: TranslateService
  ) {
    this.config = this.ngxUiLoaderService.getDefaultConfig();
    translate.addLangs(['en', 'fr']);
    translate.setDefaultLang('en');

    translate.use('fr');
  }
}
